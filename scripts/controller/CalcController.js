class CalcController {

	constructor() {

		// Variáveis são recursos que armazenam informações na memória - Atributos
		this._displayCalcEl = document.querySelector("#display");
		this._dateEl = document.querySelector("#data");
		this._timeEl = document.querySelector("#hora");
		this._currentDate;
		this.initialize();

	}

	// Funções executam uma ação e retornam um valor - Métodos

	initialize() {

		// No DOM podemos usar o innerHTML para colocar um valor do elemento selecionado

		setInterval(()=> {

		}, );


	}

	get displayTime() {
		return this._timeEl.innerHTML;
	}

	set displayTime( value ) {
		return this._timeEl.innerHTML = value;
	}

	get displayDate() {
		return this._dateEl.innerHTML;
	}

	set displayDate( value ) {
		return this._dateEl.innerHTML = value;
	}

	get displayCalc() {
		return this._displayCalcEl.innerHTML;
	}

	set displayCalc( value ) {
		return this._displayCalcEl.innerHTML = value;
	}

	get currentDate() {
		return new Date();
	}

	set currentDate( value ) {
		this._currentDate = value;
	}



}